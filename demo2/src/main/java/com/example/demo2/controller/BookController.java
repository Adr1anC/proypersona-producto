package com.example.demo2.controller;


import com.example.demo2.model.entity.Book;
import com.example.demo2.model.pojo.dto.BookDTO;
import com.example.demo2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/Book")
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<?> findAllBooks() {
        return new ResponseEntity <>(bookService.findAllBooks(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> persist(@RequestBody BookDTO dto){
        Book entity = bookService.persist(dto);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PutMapping("/{book_id}")
    public ResponseEntity <?> update(@PathVariable("book_id") int book_id, @RequestBody BookDTO dto){
        Optional<Book> entity = bookService.findById(book_id);
        entity.get().setBook_name(dto.getBook_name());
        entity.get().setPublication_date(dto.getPublication_date());
        bookService.update(entity.get());
        return new ResponseEntity<>(entity.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{book_id}")
    public ResponseEntity <?> delete(@PathVariable("book_id") int book_id){
        Optional<Book> entity = bookService.findById(book_id);
        bookService.delete(entity.get());
        return new ResponseEntity <>("eliminado",HttpStatus.OK);
    }
}
