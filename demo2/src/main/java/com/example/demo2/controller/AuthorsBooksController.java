package com.example.demo2.controller;

import com.example.demo2.model.entity.AuthorsBooks;
import com.example.demo2.model.pojo.dto.AuthorsBooksDTO;
import com.example.demo2.service.AuthorsBooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/Authors_Books")
public class AuthorsBooksController {

    private final AuthorsBooksService authorsBooksService;
    @Autowired
    public AuthorsBooksController(AuthorsBooksService authorsBooksService) {this.authorsBooksService = authorsBooksService;}

    @GetMapping
    public ResponseEntity<?> findAllAuthorsBooks() {
        return new ResponseEntity <>(authorsBooksService.findAllAuthorsBooks(), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<?> persist(@RequestBody AuthorsBooksDTO dto){
        AuthorsBooks entity = authorsBooksService.persist(dto);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PutMapping("/{author_book_id}")
    public ResponseEntity <?> update(@PathVariable("author_book_id") int author_book_id, @RequestBody AuthorsBooksDTO dto){
        Optional<AuthorsBooks> entity = authorsBooksService.findById_AuthorBook(author_book_id);
        entity.get().setAuthor_id(dto.getAuthor_id());
        entity.get().setBook_id(dto.getBook_id());
        authorsBooksService.update(entity.get());
        return new ResponseEntity<>(entity.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{author_book_id}")
    public ResponseEntity <?> delete(@PathVariable("author_book_id") int author_book_id){
        Optional<AuthorsBooks> entity = authorsBooksService.findById_AuthorBook(author_book_id);
        authorsBooksService.delete(entity.get());
        return new ResponseEntity <>("eliminado",HttpStatus.OK);
    }
}
