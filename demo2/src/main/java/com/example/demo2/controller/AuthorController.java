package com.example.demo2.controller;

import com.example.demo2.model.entity.Author;
import com.example.demo2.model.pojo.dto.AuthorDTO;
import com.example.demo2.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/Authors")
public class AuthorController {

    private final AuthorService authorService;
    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<?> findAllAuthors() {
        return new ResponseEntity <>(authorService.findAllAuthors(), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<?> persist(@RequestBody AuthorDTO dto){
        Author entity = authorService.persist(dto);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PutMapping("/{author_id}")
    public ResponseEntity <?> update(@PathVariable("author_id") int author_id, @RequestBody AuthorDTO dto){
        Optional<Author> entity = authorService.findById(author_id);
        entity.get().setName(dto.getName());
        entity.get().setLastName(dto.getLastName());
        entity.get().setLiterally_genre(dto.getLiterally_genre());
        entity.get().setNationality(dto.getNationality());
        entity.get().setBirthday(dto.getBirthday());
        authorService.update(entity.get());
        return new ResponseEntity<>(entity.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{author_id}")
    public ResponseEntity <?> delete(@PathVariable("author_id") int author_id, @RequestBody AuthorDTO dto){
        Optional<Author> entity = authorService.findById(author_id);
        authorService.delete(entity.get());
        return new ResponseEntity <>("eliminado",HttpStatus.OK);
    }

}
