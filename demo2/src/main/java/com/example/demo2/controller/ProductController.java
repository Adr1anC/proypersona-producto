package com.example.demo2.controller;

import com.example.demo2.model.repository.ProductoRepository;
import com.example.demo2.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Product")
public class ProductController {

    private final ProductoService productoService;

    @Autowired
    public ProductController(ProductoService productoService, ProductoRepository productoRepository, ProductoRepository productoRepository1) {
        this.productoService = productoService;
    }

    @GetMapping
    public ResponseEntity<?> findAllProductos() {
        return new ResponseEntity<>(productoService.findAllProductos(), HttpStatus.OK);
    }
}