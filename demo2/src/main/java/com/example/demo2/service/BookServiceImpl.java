package com.example.demo2.service;

import com.example.demo2.model.entity.Book;
import com.example.demo2.model.pojo.dto.BookDTO;
import com.example.demo2.model.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    @Autowired
    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public List findAllBooks(){
        return bookRepository.findAllBooks();
    }

    @Override
    public Book persist(BookDTO dto){
        Book entity = new Book();
        entity.setBook_name(dto.getBook_name());
        entity.setDescription(dto.getDescription());
        entity.setPublication_date(dto.getPublication_date());
        bookRepository.save(entity);
        return entity;
    }

    @Override
    public void update(Book entity) {
        bookRepository.save(entity);
    }

    @Override
    public void delete(Book entity) {bookRepository.delete(entity);}

    @Override
    public Optional<Book> findById(int id) {
        Optional<Book> entity = bookRepository.findById(id);
        return entity;
    }
}
