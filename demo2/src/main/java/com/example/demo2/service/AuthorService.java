package com.example.demo2.service;

import com.example.demo2.model.entity.Author;
import com.example.demo2.model.pojo.dto.AuthorDTO;

import java.util.List;
import java.util.Optional;

public interface AuthorService{
    List findAllAuthors();
    Author persist(AuthorDTO dto);
    void update(Author entity);
    void delete(Author entity);
    Optional<Author> findById(int id);
}