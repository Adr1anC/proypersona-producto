package com.example.demo2.service;

import com.example.demo2.model.entity.AuthorsBooks;
import com.example.demo2.model.pojo.dto.AuthorsBooksDTO;

import java.util.List;
import java.util.Optional;

public interface AuthorsBooksService {
    List<AuthorsBooks>findAllAuthorsBooks();
    AuthorsBooks persist(AuthorsBooksDTO dto);
    void update(AuthorsBooks entity);
    void delete(AuthorsBooks entity);
    Optional<AuthorsBooks> findById_AuthorBook(int author_book_id);
}