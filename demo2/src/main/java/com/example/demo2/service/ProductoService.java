package com.example.demo2.service;

import com.example.demo2.model.entity.Producto;

import java.util.List;

public interface ProductoService{
    List<Producto> findAllProductos();
}
