package com.example.demo2.service;

import com.example.demo2.model.entity.Author;
import com.example.demo2.model.pojo.dto.AuthorDTO;
import com.example.demo2.model.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List findAllAuthors(){
        return authorRepository.findAllAuthors();
    }

    @Override
    public Author persist(AuthorDTO dto){
        Author entity = new Author();
        entity.setName(dto.getName());
        entity.setLastName(dto.getLastName());
        entity.setLiterally_genre(dto.getLiterally_genre());
        entity.setNationality(dto.getNationality());
        entity.setBirthday(dto.getBirthday());
        authorRepository.save(entity);
        return entity;
    }

    @Override
    public void update(Author entity) {
        authorRepository.save(entity);
    }

    @Override
    public void delete(Author entity) {authorRepository.delete(entity);}

    @Override
    public Optional<Author> findById(int author_id) {
        Optional<Author> entity = authorRepository.findById(author_id);
        return entity;
    }

}
