package com.example.demo2.service;

import com.example.demo2.model.entity.AuthorsBooks;
import com.example.demo2.model.pojo.dto.AuthorsBooksDTO;
import com.example.demo2.model.repository.AuthorsBooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorsBooksServiceImpl implements AuthorsBooksService{
    private final AuthorsBooksRepository authorsBooksRepository;
    @Autowired
    public AuthorsBooksServiceImpl(AuthorsBooksRepository authorsBooksRepository) {this.authorsBooksRepository = authorsBooksRepository;}

    @Override
    public List<AuthorsBooks> findAllAuthorsBooks(){
        return authorsBooksRepository.findAll();
    }

    @Override
    public AuthorsBooks persist(AuthorsBooksDTO dto){
        AuthorsBooks entity = new AuthorsBooks();
        entity.setAuthor_id(dto.getAuthor_id());
        entity.setBook_id(dto.getBook_id());
        authorsBooksRepository.save(entity);
        return entity;
    }

    @Override
    public void update(AuthorsBooks entity) {
        authorsBooksRepository.save(entity);
    }

    @Override
    public void delete(AuthorsBooks entity) {authorsBooksRepository.delete(entity);}

    @Override
    public Optional<AuthorsBooks> findById_AuthorBook(int author_book_id){
        Optional<AuthorsBooks> entity = authorsBooksRepository.findById(author_book_id);
        return entity;
    }
}
