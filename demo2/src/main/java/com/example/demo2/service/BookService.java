package com.example.demo2.service;

import com.example.demo2.model.entity.Book;
import com.example.demo2.model.pojo.dto.BookDTO;

import java.util.Optional;

public interface BookService {
    Object findAllBooks();

    Book persist(BookDTO dto);

    Optional<Book> findById(int book_id);

    void update(Book book);

    void delete(Book book);
}
