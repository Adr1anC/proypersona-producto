package com.example.demo2.model.pojo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class AuthorDTO {
    private String name;
    private String lastName;
    private String literally_genre;
    private String nationality;
    private Date birthday;
}
