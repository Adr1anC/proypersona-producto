package com.example.demo2.model.repository;


import com.example.demo2.model.entity.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer>{
    @Query(value= "SELECT author_id, birthday, last_name, literally_genre, name, nationality\n" +
            "\tFROM public.author;", nativeQuery = true)
    List findAllAuthors();

}
