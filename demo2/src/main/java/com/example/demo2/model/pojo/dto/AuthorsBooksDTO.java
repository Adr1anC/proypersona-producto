package com.example.demo2.model.pojo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class AuthorsBooksDTO{
    private Integer author_id;
    private Integer book_id;
}
