package com.example.demo2.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(schema = "public",name="Book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private int book_id;
    @Column(name = "book_name")
    private String book_name;
    @Column(name = "description")
    private String description;
    @Column(name = "publication_date")
    @Temporal(TemporalType.DATE)
    private Date publication_date;

    @ManyToMany(mappedBy = "books", cascade = {CascadeType.ALL})
    private Set<Author> Author = new HashSet<Author>();
}