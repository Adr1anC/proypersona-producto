package com.example.demo2.model.repository;


import com.example.demo2.model.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer>{
    @Query(value = "SELECT book_id, book_name, description, publication_date\n" +
            "\tFROM public.book;", nativeQuery = true)
    List findAllBooks();
}