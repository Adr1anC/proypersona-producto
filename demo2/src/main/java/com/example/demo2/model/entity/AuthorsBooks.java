package com.example.demo2.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "authors_books")
public class AuthorsBooks{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "author_book_id")
    private int book_author_id;
    @Column(name = "author_id")
    private int author_id;
    @Column(name = "book_id")
    private int book_id;
}