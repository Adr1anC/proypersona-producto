package com.example.demo2.model.pojo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoDTO {
    private String name;
    private String prize;
}
