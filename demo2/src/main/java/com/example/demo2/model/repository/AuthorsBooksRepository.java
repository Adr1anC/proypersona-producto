package com.example.demo2.model.repository;

import com.example.demo2.model.entity.AuthorsBooks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorsBooksRepository extends JpaRepository<AuthorsBooks, Integer>{
}
