package com.example.demo2.model.pojo.dto;

import com.example.demo2.model.entity.Author;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class BookDTO {
    private String book_name;
    private String description;
    private Date publication_date;
}
