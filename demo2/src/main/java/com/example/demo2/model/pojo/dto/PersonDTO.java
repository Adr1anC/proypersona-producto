package com.example.demo2.model.pojo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDTO {
    private String name;
    private String lastName;
    private String document;
}
