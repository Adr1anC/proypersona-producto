package com.example.demo2.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(schema = "public",name="Author")
public class Author{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "author_id")
    private int author_id;
    @Column(name="name")
    private String name;
    @Column(name="lastName")
    private String lastName;
    @Column(name="literally_genre")
    private String literally_genre;
    @Column(name="nationality")
    private String nationality;
    @Column(name="birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
        name = "authors_books",
            joinColumns = {@JoinColumn(name = "author_id")},
        inverseJoinColumns = {@JoinColumn(name = "book_id")})
    private Set<Book> books = new HashSet<Book>();
}

